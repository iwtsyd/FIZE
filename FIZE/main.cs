﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIZE
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
            loading(); // Загрузка формы
        }

        private void loading()
        {
            // Отключение кнопки "Максимизировать" в заголовке окна
            this.MaximizeBox = false;

            // Запуск окна по центру экрана
            this.StartPosition = FormStartPosition.CenterScreen;

            // Установка темного фона окна
            this.BackColor = Color.FromArgb(30, 30, 30);

            // Установка белого цвета текста для элемента author
            author.ForeColor = Color.White;

            // Установка белого цвета текста для элемента selectFileText
            selectFileText.ForeColor = Color.White;

            // Установка белого цвета текста для элемента sizeText
            sizeText.ForeColor = Color.White;

            // Установка белого цвета текста для элемента mbText
            mbText.ForeColor = Color.White;

            // Установка темного фона и белого цвета текста для текстового поля filePathBox
            filePathBox.BackColor = Color.FromArgb(38, 38, 38);
            filePathBox.ForeColor = Color.White;

            // Установка темного фона и белого цвета текста для текстового поля fileSizeBox
            fileSizeBox.BackColor = Color.FromArgb(38, 38, 38);
            fileSizeBox.ForeColor = Color.White;

            // Скрытие значка приложения в заголовке окна
            this.ShowIcon = false;
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog opd = new OpenFileDialog())
            {
                // Сброс выбранного файла
                opd.FileName = null;

                // Установка фильтра для диалогового окна выбора файла
                opd.Filter = "Все файлы|*.*";

                // Установка заголовка диалогового окна выбора файла
                opd.Title = $"{this.Text} - Выберите файл";

                // Отображение диалогового окна выбора файла и проверка результата
                if (opd.ShowDialog() == DialogResult.Cancel)
                    return; // Если нажата кнопка "Отмена", выйти из метода

                // Присвоение пути выбранного файла текстовому полю filePathBox
                filePathBox.Text = opd.FileName;
            }

        }

        private void fileSizeBox_TextChanged(object sender, EventArgs e)
        {
            foreach (char x in fileSizeBox.Text)
            {
                // Проверка, является ли текущий символ цифрой
                if (!Char.IsDigit(x))
                {
                    // Если символ не является цифрой, очищаем содержимое текстового поля fileSizeBox
                    fileSizeBox.Text = null;
                    break; // Прекращаем цикл, так как уже обнаружен недопустимый символ
                }
            }
        }

        // Конвертирует мегабайты в байты.
        private static int ConvertMegabytesToBytes(int megabytes)
        {
            // Вычисление эквивалентного количества байтов путем умножения мегабайтов на множитель 1024 * 1024
            int bytes = (megabytes * 1024 * 1024);

            return bytes;
        }




        // Генерирует массив случайных байтов указанного размера.
        private static byte[] GenerateRandomData(int size)
        {
            // Создание массива для хранения случайных байтов заданного размера
            byte[] data = new byte[size];

            // Генерация случайных байтов и заполнение массива
            new Random().NextBytes(data);

            return data;
        }

        private void darkButton1_Click(object sender, EventArgs e)
        {
            if (filePathBox.Text == null)
                return;

            // Преобразование значения из текстового поля fileSizeBox в целочисленное значение
            int additionalSize = ConvertMegabytesToBytes(int.Parse(fileSizeBox.Text));

            // Создание массива случайных байтов указанного размера
            byte[] randomData = GenerateRandomData(additionalSize);

            var message = MessageBox.Show($"Вы точно хотите увеличть размер файла на {fileSizeBox.Text}MB?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (message == DialogResult.No)
                return;

            try
            {
                using (FileStream fs = new FileStream(filePathBox.Text, FileMode.Append))
                {
                    byte[] buffer = new byte[1024]; // Буфер для записи данных (1 КБ)
                    Random random = new Random();

                    while (additionalSize > 0)
                    {
                        int bufferSize = Math.Min(additionalSize, buffer.Length);
                        random.NextBytes(buffer);
                        fs.Write(buffer, 0, bufferSize);
                        additionalSize -= bufferSize;
                    }                 
                }

                MessageBox.Show($"Файл был увеличен на {fileSizeBox.Text}MB", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Restart();
            }
            catch (Exception ex)
            {  }
        }
    }
}
