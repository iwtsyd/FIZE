﻿namespace FIZE
{
    partial class main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.author = new System.Windows.Forms.Label();
            this.selectFileText = new System.Windows.Forms.Label();
            this.filePathBox = new System.Windows.Forms.TextBox();
            this.selectButton = new FIZE.DarkButton();
            this.sizeText = new System.Windows.Forms.Label();
            this.fileSizeBox = new System.Windows.Forms.TextBox();
            this.darkButton1 = new FIZE.DarkButton();
            this.mbText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // author
            // 
            this.author.AutoSize = true;
            this.author.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.author.Location = new System.Drawing.Point(457, 224);
            this.author.Name = "author";
            this.author.Size = new System.Drawing.Size(66, 17);
            this.author.TabIndex = 0;
            this.author.Text = "by 1wtsyd";
            // 
            // selectFileText
            // 
            this.selectFileText.AutoSize = true;
            this.selectFileText.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectFileText.Location = new System.Drawing.Point(10, 18);
            this.selectFileText.Name = "selectFileText";
            this.selectFileText.Size = new System.Drawing.Size(192, 32);
            this.selectFileText.TabIndex = 1;
            this.selectFileText.Text = "Выберите файл: ";
            // 
            // filePathBox
            // 
            this.filePathBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.filePathBox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.filePathBox.Location = new System.Drawing.Point(16, 57);
            this.filePathBox.Name = "filePathBox";
            this.filePathBox.Size = new System.Drawing.Size(460, 26);
            this.filePathBox.TabIndex = 2;
            // 
            // selectButton
            // 
            this.selectButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.selectButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this.selectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectButton.Font = new System.Drawing.Font("Arial", 11F);
            this.selectButton.ForeColor = System.Drawing.Color.White;
            this.selectButton.Location = new System.Drawing.Point(485, 57);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(26, 26);
            this.selectButton.TabIndex = 3;
            this.selectButton.Text = "?";
            this.selectButton.UseVisualStyleBackColor = false;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // sizeText
            // 
            this.sizeText.AutoSize = true;
            this.sizeText.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sizeText.Location = new System.Drawing.Point(10, 100);
            this.sizeText.Name = "sizeText";
            this.sizeText.Size = new System.Drawing.Size(198, 32);
            this.sizeText.TabIndex = 4;
            this.sizeText.Text = "Введите размер: ";
            // 
            // fileSizeBox
            // 
            this.fileSizeBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fileSizeBox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fileSizeBox.Location = new System.Drawing.Point(201, 106);
            this.fileSizeBox.Name = "fileSizeBox";
            this.fileSizeBox.Size = new System.Drawing.Size(273, 26);
            this.fileSizeBox.TabIndex = 5;
            this.fileSizeBox.TextChanged += new System.EventHandler(this.fileSizeBox_TextChanged);
            // 
            // darkButton1
            // 
            this.darkButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.darkButton1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this.darkButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.darkButton1.Font = new System.Drawing.Font("Arial", 11F);
            this.darkButton1.ForeColor = System.Drawing.Color.White;
            this.darkButton1.Location = new System.Drawing.Point(16, 160);
            this.darkButton1.Name = "darkButton1";
            this.darkButton1.Size = new System.Drawing.Size(492, 53);
            this.darkButton1.TabIndex = 6;
            this.darkButton1.Text = "Увеличить";
            this.darkButton1.UseVisualStyleBackColor = false;
            this.darkButton1.Click += new System.EventHandler(this.darkButton1_Click);
            // 
            // mbText
            // 
            this.mbText.AutoSize = true;
            this.mbText.Font = new System.Drawing.Font("Segoe UI Semilight", 14F);
            this.mbText.Location = new System.Drawing.Point(477, 106);
            this.mbText.Name = "mbText";
            this.mbText.Size = new System.Drawing.Size(39, 25);
            this.mbText.TabIndex = 7;
            this.mbText.Text = "MB";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 244);
            this.Controls.Add(this.mbText);
            this.Controls.Add(this.darkButton1);
            this.Controls.Add(this.fileSizeBox);
            this.Controls.Add(this.sizeText);
            this.Controls.Add(this.selectButton);
            this.Controls.Add(this.filePathBox);
            this.Controls.Add(this.selectFileText);
            this.Controls.Add(this.author);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "main";
            this.Text = "FIZE";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label author;
        private System.Windows.Forms.Label selectFileText;
        private System.Windows.Forms.TextBox filePathBox;
        private DarkButton selectButton;
        private System.Windows.Forms.Label sizeText;
        private System.Windows.Forms.TextBox fileSizeBox;
        private DarkButton darkButton1;
        private System.Windows.Forms.Label mbText;
    }
}

