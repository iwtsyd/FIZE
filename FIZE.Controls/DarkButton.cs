﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace FIZE
{
    public partial class DarkButton : Button
    {
        public DarkButton()
        {
            InitializeComponent();
            FlatStyle = FlatStyle.Flat;
            FlatAppearance.BorderColor = Color.FromArgb(110, 110, 110);
            BackColor = Color.FromArgb(38, 38, 38);
            ForeColor = Color.White;
            Font = new Font("Arial", 11, FontStyle.Regular | FontStyle.Regular);
        }
    }
}
